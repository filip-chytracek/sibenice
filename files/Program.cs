﻿using System;
using System.Linq;
using System.Xml.Linq;
using System.IO;
using System.Collections.Generic;

namespace Sibenice
{
    class Program
    {

        static class UserState
        {
            public static string wordToGuess = "";
            public static int lifesLeft = 20;
            public static int wordMaxLength = 0;
            public static string guessedChars = "";
        }

        //Types and Enums

        enum MenuEnum
        {
            start,
            guide,
            list,
            exit,
        }

        enum DiffucultyEnum
        {
            easy,
            medium,
            hard,
        }

        // Console functions

        // custom funkce pro čtení inputu
        static string ReadUserInput()
        {
            string userInput = Console.ReadLine();
            return userInput;
        }

        // custom funkce pro čtění charu
        static char ReadChar()
        {
            char userChar = Console.ReadKey().KeyChar;
            return userChar;
        }

        // custom funkce pro logging
        static void LogText(string text, object variable = null)
        {
            Console.WriteLine(text + variable);
        }

        // čtení hlavního meny
        static void ReadUserMenuInput()
        {
            string userInput = ReadUserInput();
            MenuEnum userInputEnumValue;
            if (!MenuEnum.TryParse<MenuEnum>(userInput, out userInputEnumValue))
            {
                LogText("Špatné slovo, zkus jedno z menu možností.");
                ReadUserMenuInput();
                return;
            }
            switch ((MenuEnum)Enum.Parse(typeof(MenuEnum), userInput, false))
            {
                case MenuEnum.start:
                    StartGame();
                    break;
                case MenuEnum.list:
                    OpenWordsList();
                    break;
                case MenuEnum.guide:
                    OpenGuide();
                    break;
                case MenuEnum.exit:
                    Environment.Exit(0);
                    break;
            }
        }

        // čtení obtížnosti hry
        static void ReadDifficultyInput()
        {
            string userInput = ReadUserInput();
            DiffucultyEnum userInputEnumValue;
            if (!DiffucultyEnum.TryParse<DiffucultyEnum>(userInput, out userInputEnumValue))
            {
                LogText("Špatné slovo, zkus jedno z menu možností.");
                ReadDifficultyInput();
                return;
            }
            switch ((DiffucultyEnum)Enum.Parse(typeof(DiffucultyEnum), userInput, true))
            {
                case DiffucultyEnum.easy:
                    UserState.lifesLeft = 15;
                    UserState.wordMaxLength = 5;
                    break;
                case DiffucultyEnum.medium:
                    UserState.lifesLeft = 10;
                    UserState.wordMaxLength = 0;
                    break;
                case DiffucultyEnum.hard:
                    UserState.lifesLeft = 5;
                    UserState.wordMaxLength = 0;
                    break;
            }
        }

        // File functionality
        // načtení XML
        static List<string> LoadDictionary()
        {
            string filename = "dictionary.xml";
            string purchaseOrderFilepath = Path.Combine("../../../", filename);

            XElement dictionary = XElement.Load(purchaseOrderFilepath);
            IEnumerable<XElement> words = from item in dictionary.Elements("Word")
                                          select item;
            List<string> wordsList = new List<string>();

            foreach (XElement word in words)
            {
                wordsList.Add(word.Value.ToLower());

            }

            return wordsList;
        }

        //vybrání random slova z xml
        static void ChooseRandomWord()
        {
            List<string> words = LoadDictionary();


            Random randomIndex = new Random();
            List<string> filteredWords = new List<string>();


            foreach (string word in words)
            {
                switch (UserState.wordMaxLength)
                {
                    case 0:
                        if (word.Length >= 5)
                        {
                            filteredWords.Add(word);
                        }
                        break;
                    case 5:
                        if (word.Length <= UserState.wordMaxLength)
                        {
                            filteredWords.Add(word);
                        }
                        break;

                    default:
                        filteredWords.Add(word);
                        break;
                }
            }
            int randomFilteredIndex = randomIndex.Next(filteredWords.Count);
            UserState.wordToGuess = filteredWords[randomFilteredIndex];
        }

        // otevření nápovědy ke hře
        static void OpenGuide()
        {
            LogText("\nVítej ve hře šibenice.\n\nPří zvolení možnosti `start` v hlavním meny spustíš hru\npo spuštění hry bude vybráno náhodné slovo a tvým cílem je ho uhodnout!\nHádáš jednotlivá písmenka a máš omezený počet životů, v případě že ti dojdou životy nebo slovo uhodneš, hra skončí.\n Pozor, hra bere v potaz diakritiku!\n\n\t Hodně štěstí!");
            BackToMenu();
        }

        // vypíše seznam všech slov z XML filu
        static void OpenWordsList()
        {
            List<string> words = LoadDictionary();

            foreach (string word in words)
            {
                LogText(word);

            }
            BackToMenu();
        }


        // Game functionality

        // vrácení do meny
        static void BackToMenu()
        {
            LogText("________________________________________________________");
            LogText("Pro návrat do menu stiskni libovolnou klávesu");
            if (ReadChar() != null)
            {
                OpenMenu(false);
            }
        }

        // otevření star menu
        static void OpenMenu(Boolean showTitle)
        {
            if (showTitle)
            {
                LogText("\tVítej ve hře Šibenice!");
                LogText("------------------------------------------");
            }
            LogText("\n");
            LogText("Menu");
            LogText(" 1. Pro začnutí hry napiš `start`");
            LogText(" 2. Pro vylistování slov napiš `list`");
            LogText(" 3. Pro ukázání pravidel hry napiš `guide`");
            LogText(" 4. Pro opuštění hry napiš `exit`");

            ReadUserMenuInput();
        }

        // vytvoření placeholderu pro zobrazení uživateli v průběhu hádání
        static void CreatePlaceholderForGuessedChars()
        {
            string wordToGuess = UserState.wordToGuess;
            string placeholder = "";
            for (int i = 0; i < wordToGuess.Length; i++)
            {
                placeholder = placeholder + "_";
            }
            UserState.guessedChars = placeholder;
        }

        // vymění daný uhodnutý znak za znak v placeholderu aby ho uživateli odkryly
        static void ReplacePlaceholderWithGuessedChar(char guessedChar)
        {
            string newString = "";
            string localWordToGuess = UserState.wordToGuess;
            string userStateGuessedChars = UserState.guessedChars;
            List<int> indexesOfGuessedChar = new List<int>();
            for (int i = 0; i < localWordToGuess.Length; i++)
            {
                if (localWordToGuess[i] == guessedChar)
                {
                    indexesOfGuessedChar.Add(i);
                }
            }

            indexesOfGuessedChar.Reverse();
            newString = userStateGuessedChars;
            foreach (int index in indexesOfGuessedChar)
            {
                newString = newString.Remove(index, 1).Insert(index, guessedChar.ToString());
            }
            UserState.guessedChars = newString;
        }

        // ošetření uhodnutého písmena
        static void HandleGuess(char usersGuess)
        {
            LogText("\n\n___________________________________________");
            if (usersGuess == '!')
            {
                OpenMenu(false);
                return;
            }
            if (UserState.guessedChars.Contains(usersGuess))
            {
                LogText("Toto písmeno už jsi hádal, zkus znovu");
                return;
            }
            if (UserState.wordToGuess.Contains(usersGuess))
            {

                LogText("Skvěle, toto písmeno je správné!");
                ReplacePlaceholderWithGuessedChar(usersGuess);
                string guessedCharsWithSpacing = "";
                for (int i = 0; i < UserState.guessedChars.Length; i++)
                {
                    guessedCharsWithSpacing = guessedCharsWithSpacing + UserState.guessedChars[i] + " ";
                }
                LogText("Uhodneté znaky: ", guessedCharsWithSpacing);
                return;
            }


            UserState.lifesLeft--;
            LogText("Špatný tip, zkus znovu.");
            LogText("Zbývající životy: ", UserState.lifesLeft);


        }

        // nastavení obtížnosti hry
        static void SetDifficulty()
        {
            LogText("\nVyber si obtíznost\n\nNapiš `easy` pro 15 životů a slovo max 5 znaků dlouhé.\nNapiš `medium` pro 10 životů a slovo delší jak 5 znaků.\nNapiš `hard` pro 5 životů a slovo delší jak 5 znaků.");
            ReadDifficultyInput();
        }

        //fce pro spuštění hry, inicialuzje třídu usera, čeká na input
        static void StartGame()
        {
            SetDifficulty();
            //Initiallaze game
            ChooseRandomWord();
            CreatePlaceholderForGuessedChars();

            LogText("\n");
            LogText("Začni hádat");
            LogText("Tvé slovo má délku: ", UserState.wordToGuess.Length);

            while (UserState.lifesLeft > 0 && UserState.guessedChars != UserState.wordToGuess)
            {
                LogText("\nNapiš `!` aby ses dostal zpět do menu");
                char usersGuess = ReadChar();
                HandleGuess(usersGuess);
            }

            if (UserState.guessedChars == UserState.wordToGuess)
            {
                LogText("Uhodl jsi slovo: ", UserState.wordToGuess);
            }

            if (UserState.lifesLeft == 0)
            {
                LogText("Došli životy!");
                LogText("Neuhdl jsi slovo: ", UserState.wordToGuess);
            }
            BackToMenu();
        }

        static void Main(string[] args)
        {
            //initialization of UserState
            OpenMenu(true);
        }
    }
}